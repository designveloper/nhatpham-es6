let a = 1;
a = 3; // a can not redeclare 
let showname = (name) => {
  {
    let say = 'your name is ' + name;
  }
  console.log(say); // say can not use in this scope
}
showname('nhat');
