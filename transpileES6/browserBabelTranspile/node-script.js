var EventEmitter = require('events').EventEmitter;
var Person = function(name){
  this.name = name;
};
Person.prototype = new EventEmitter();
let mn = new Person('Minh Nhat');
mn.on('speak',function(say){
    console.log('nhat say:'+ say);
});
mn.emit('speak', 'A du');